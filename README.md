# **Books API Test Automation Solution**
API Automation with Rest-Assured, Java 8, Maven, Serenity BDD Framework and Gitlab-ci

## Requirements:
Below dependencies needs to be installed/configured
- Java 8 or higher (JAVA_HOME and PATH in environmental variables)
- Maven (M2, MAVEN_HOME and PATH in environmental variables)
- IDE
- It would be nice to have "Cucumber for Java" and "Gherkin" plugins installed in the IDE for better understanding and working of cucumber

```
src
  + test
    + java                                       Test runners and supporting code
      + actions                                  Reusable step methods
      + config                                   Properties loader file
      + constants                                Enum with api end points
      + runners                                  Test runner class
      + models                                   Models (Pojo classes for storing json responses)
      + steps                                    Step definitions corresponding to BDD feature files
    + resources
      + features                                 Feature files directory
         BestSellersHistory.feature              Feature containing BDD scenarios
         BookListNames.feature                   Feature containing BDD scenarios
         BooksListNamesNegativeScenarios.feature Feature containing BDD scenarios
```
### Install and executing the tests

- Navigate to project root folder using "cd <project path" in the command line.
- Run `mvn clean verify` from the command line.

The test results will be recorded in folder: `target/site/serenity/index.html`.

##### Testrunner
To start the test execution, test runner is needed. It is the starting point for Junit to start the test execution. 
In the project it is defined in the "runners" folder. File name is TestRunner.java.

```
import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        features = "src/test/resources/features",
        glue = {"com.books.api.steps"}
)
public class TestRunner {
}
```
##### Feature files
- Make a new .feature file in the folder: resources/features
  Define the test cases in the Given, When, Then format
  
  ```Gherkin
  Scenario: test scenario
    (Given) some context
    (When) some action that needs to be carried out
    (Then) a particular set of observable consequences or outcome
  ```
  Using tables in the feature file is a nice solution to store data used for the tests.
  It is also easy to make new test cases simply by adding a new row
  
##### Step definitions
- Make a new .java file in the folder: steps.
- Define the test scenario steps from the feature file to 'glue' them with the actual code.
- For each feature file a corresponding step definition is created.
- The steps which are generic to all feature files were added in "GenericSteps.java" file.
  
### Endpoint overview 
####/lists.json 
-- This end point returns the list of books with the corresponding details. 
For this end point list_name end point is mandatory and remaining fields are optional.
####/lists/best-sellers/history.json
-- Returns list of best seller books
####/lists/names.json
-- Returns all the book list names.
For more information regarding the end points, please refer to https://developer.nytimes.com/

### CI pipeline Gitlab
```
- All the tests will automatically run, if there are any new changes pushed to the Gitlab repo.
- Gitlab repo url is https://gitlab.com/gnpavan430/apiserene.git.
- All commands are configure in .gitlab-ci.yml file, so that the project can directly run via gitlab ci pipeline.
- Test artificats will be available after test run at path "target/site/serenity/"
```



