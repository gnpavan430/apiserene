package com.books.api.constants;

// This enum is used for storing api end points
public enum APIEndPoint {
    BooksList("/lists.json"),
    BestSellersHistory("/lists/best-sellers/history.json"),
    ListOfNames("/lists/names.json"),
    ListOverview("/lists/overview.json"),
    ListDate("lists//.json"),
    Reviews("reviews.json");

    private String endPoint;

    APIEndPoint(String resource)
    {
        this.endPoint = resource;
    }

    public String getEndPoint() {
        return endPoint;
    }
}
