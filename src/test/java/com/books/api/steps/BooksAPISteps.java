package com.books.api.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import com.books.api.models.Books;
import org.hamcrest.beans.HasPropertyWithValue;
import org.hamcrest.core.Every;
import org.hamcrest.core.Is;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class BooksAPISteps extends BaseTest {
    private Books books;

    @Then("response is stored in books object")
    public void result_should_have_status_as() {
        books = response.extract().as(Books.class);
    }

    @Then("book api result should have status as {string}")
    public void result_should_have_status_as(String status) {
        assertThat("Status is OK", books.getStatus(), equalTo(status));
    }

    @Then("number of books should be greater than {int}")
    public void number_of_books_should_be_greater_than(Integer count) {
        assertThat("Number of books count", books.getNumResults(), greaterThan(count));
    }

    @Then("all the books should have list name as {string}")
    public void all_the_books_should_have_list_name_as(String bookListName) {
        assertThat("Validating list names of all books", books.getBookResults(), (Every.everyItem(HasPropertyWithValue.hasProperty("listName", Is.is(bookListName)))));
    }

    @And("number of books returned should be {int}")
    public void numberOfBooksReturnedShouldBe(int numberOfBooks) {
        assertThat(books.getNumResults(), equalTo(numberOfBooks));
    }

}
