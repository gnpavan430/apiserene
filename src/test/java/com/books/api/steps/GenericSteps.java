package com.books.api.steps;

import com.books.api.actions.APIActions;
import com.books.api.constants.APIEndPoint;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import net.thucydides.core.annotations.Steps;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.containsString;

public class GenericSteps extends BaseTest {

    @Steps
    APIActions apiActions;

    Map<String, String> queryParams = new HashMap<>();
    Map<String, String> headers = new HashMap<>();

    @Given("a valid books api sessions is created")
    public void the_request_with_valid_book_name_to_the_Books_api_endpoint() {
        queryParams.put("api-key", getProperties().getProperty("apiKey"));
    }

    @Given("query params are added to the session")
    public void add_query_params_to_the_session(DataTable dataTable) {
        for(Map<String, String> values: dataTable.asMaps()) {
            queryParams.put(values.get("key"), values.get("value"));
        }
    }

    @Given("headers are added to the session")
    public void headers_are_added_to_the_session(DataTable dataTable) {
        for(Map<String, String> values: dataTable.asMaps()) {
            headers.put(values.get("key"), values.get("value"));
        }
    }

    @When("the GET request is made to the api endpoint {string}")
    public void the_request_is_made_with_get_method_to_Books_api_endpoint(String apiEndPoint) {
        APIEndPoint constants = APIEndPoint.valueOf(apiEndPoint);
        apiActions.requestAPIWithGet(queryParams, headers, constants.getEndPoint());
        response = apiActions.getResponse();
    }

    @Then("Response content type should be {string}")
    public void response_content_type_should_be_json(String applicationType) {
        assertThat(response.extract().contentType(), equalTo(applicationType));
    }

    @Then("Response should have status code as {int}")
    public void response_should_have_status_code_as(Integer statusCode) {
        assertThat(response.extract().statusCode(), equalTo(statusCode));
    }

    @Then("Response parameter {string} should contain the message {string}")
    public void result_should_contain_error_message(String parameter, String message) {
        response.assertThat().body(parameter, containsString(message));

    }

    @Then("Response parameter list {string} has item {string}")
    public void response_parameter_list_has_item(String listName, String itemName) {
        response.assertThat().body(listName, hasItem(containsString(itemName)));
    }
}
