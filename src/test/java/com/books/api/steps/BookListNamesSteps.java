package com.books.api.steps;

import cucumber.api.java.en.Then;
import com.books.api.models.BookListNames;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class BookListNamesSteps extends BaseTest {
    private BookListNames bookListNames;

    @Then("response is stored in books list name object")
    public void result_should_have_status_as() {
        bookListNames = response.extract().as(BookListNames.class);
    }

    @Then("book list names api result should have status as {string}")
    public void result_should_have_status_as(String status) {
        assertThat("Status is OK", bookListNames.getStatus(), equalTo(status));
    }

    @Then("number of book list names should be greater than {int}")
    public void number_of_books_should_be_greater_than(Integer count) {
        assertThat("Number of books count", bookListNames.getNumResults(), greaterThan(count));
    }

    @Then("response field {string} with value {string} is present in the response")
    public void response_field_with_value_is_present_in_the_response(String fieldName, String fieldValue) {
        assertThat("Checking the property value", bookListNames.getBookListNameResults(), hasItem(
                hasProperty(fieldName, is(fieldValue))
        ));

    }
}
