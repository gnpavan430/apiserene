package com.books.api.steps;

import cucumber.api.java.en.Then;
import com.books.api.models.BestSellerHistory;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class BestSellersHistorySteps extends BaseTest {
    private BestSellerHistory bestSellerHistory;

    @Then("response is stored in bestsellers history object")
    public void result_should_have_status_as() {
        bestSellerHistory = response.extract().as(BestSellerHistory.class);
    }

    @Then("bestsellers history api result should have status as {string}")
    public void result_should_have_status_as(String status) {
        assertThat("Status is OK", bestSellerHistory.getStatus(), equalTo(status));
    }

    @Then("number of best seller books is equal to {int}")
    public void number_of_books_should_be_greater_than(Integer count) {
        assertThat("Number of books count", bestSellerHistory.getNumResults(), equalTo(count));
    }

    @Then("best seller history response field {string} with value {string} is present in the response")
    public void response_field_with_value_is_present_in_the_response(String fieldName, String fieldValue) {
        assertThat("Checking the property value", bestSellerHistory.getBestSellerDetails(), hasItem(
                hasProperty(fieldName, containsString(fieldValue))
        ));

    }
}
