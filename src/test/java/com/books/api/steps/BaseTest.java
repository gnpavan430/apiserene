package com.books.api.steps;

import com.books.api.config.PropertiesLoader;
import io.restassured.response.ValidatableResponse;

import java.util.Properties;

public class BaseTest {
    public static final String BASE_URL = "https://api.nytimes.com/svc/books/v3";
    protected static ValidatableResponse response;
    private Properties properties;

    public Properties getProperties() {
        properties = PropertiesLoader.getProperties();

        return properties;
    }

}
