package com.books.api.actions;

import com.books.api.steps.BaseTest;
import io.restassured.response.ValidatableResponse;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import java.util.Map;

public class APIActions {

    public static ValidatableResponse response;

    // This method is used to make the api request
    @Step
    public void requestAPIWithGet(Map<String, String> queryParams, Map<String, String> headers, String apiEndPoint) {
        response = SerenityRest.given()
                .baseUri(BaseTest.BASE_URL)
                .queryParams(queryParams)
                .headers(headers)
                .when()
                .get(apiEndPoint).then();
    }

    // This method returns the response object to the step defintions
    @Step
    public ValidatableResponse getResponse() {
        return response;
    }

}
