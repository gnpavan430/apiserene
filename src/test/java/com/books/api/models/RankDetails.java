package com.books.api.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RankDetails {
    @JsonProperty("primary_isbn10")
    private String primary_isbn10;
    @JsonProperty("primary_isbn13")
    private String primary_isbn13;
    @JsonProperty("rank")
    private String rank;
    @JsonProperty("list_name")
    private String listName;
    @JsonProperty("display_name")
    private String displayName;
    @JsonProperty("published_date")
    private String publishedDate;
    @JsonProperty("bestsellers_date")
    private String bestSellersDate;
    @JsonProperty("weeks_on_list")
    private String weeksOnList;
    @JsonProperty("ranks_last_week")
    private String ranksLastWeek;
    @JsonProperty("asterisk")
    private String asterisk;
    @JsonProperty("dagger")
    private String dagger;

    public String getPrimary_isbn10() {
        return primary_isbn10;
    }

    public String getPrimary_isbn13() {
        return primary_isbn13;
    }

    public String getRank() {
        return rank;
    }

    public String getListName() {
        return listName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public String getBestSellersDate() {
        return bestSellersDate;
    }

    public String getWeeksOnList() {
        return weeksOnList;
    }

    public String getRanksLastWeek() {
        return ranksLastWeek;
    }

    public String getAsterisk() {
        return asterisk;
    }

    public String getDagger() {
        return dagger;
    }
}
