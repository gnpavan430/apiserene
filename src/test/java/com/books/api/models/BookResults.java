package com.books.api.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class BookResults {

    @JsonProperty("list_name")
    private String listName;
    @JsonProperty("display_name")
    private String displayName;
    @JsonProperty("bestsellers_date")
    private String bestSellersDate;
    @JsonProperty("published_date")
    private String publishedDate;

    @JsonProperty("rank")
    private int rank;
    @JsonProperty("rank_last_week")
    private int rankLastWeek;
    @JsonProperty("weeks_on_list")
    private int weeksOnList;
    @JsonProperty("asterisk")
    private int asterisk;

    @JsonProperty("dagger")
    private int dagger;
    @JsonProperty("amazon_product_url")
    private String amazon_product_url;

    @JsonProperty("isbns")
    private List<Isbns> isbns;

    @JsonProperty("book_details")
    private List<BookDetails> bookDetails;

    @JsonProperty("reviews")
    private List<Reviews> reviews;

    @JsonAnyGetter
    public int getAsterisk() {
        return asterisk;
    }

    @JsonAnySetter
    public void setAsterisk(int asterisk) {
        this.asterisk = asterisk;
    }

    @JsonAnyGetter
    public int getDagger() {
        return dagger;
    }

    @JsonAnyGetter
    public String getAmazon_product_url() {
        return amazon_product_url;
    }

    @JsonAnyGetter
    public List<Isbns> getIsbns() {
        return isbns;
    }

    @JsonAnyGetter
    public List<BookDetails> getBookDetails() {
        return bookDetails;
    }

    @JsonAnyGetter
    public List<Reviews> getReviews() {
        return reviews;
    }

    public String getListName() {
        return listName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getBestSellersDate() {
        return bestSellersDate;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public int getRank() {
        return rank;
    }

    public int getRankLastWeek() {
        return rankLastWeek;
    }

    public int getWeeksOnList() {
        return weeksOnList;
    }
}
