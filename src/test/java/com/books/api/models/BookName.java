package com.books.api.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BookName {

    @JsonProperty("list_name")
    private String listName;
    @JsonProperty("display_name")
    private String displayName;
    @JsonProperty("list_name_encoded")
    private String listNameEncoded;
    @JsonProperty("oldest_published_date")
    private String oldestPublishedDate;
    @JsonProperty("newest_published_date")
    private String newestPublishedDate;
    @JsonProperty("updated")
    private String updated;

    public String getUpdated() {
        return updated;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getListNameEncoded() {
        return listNameEncoded;
    }

    public void setListNameEncoded(String listNameEncoded) {
        this.listNameEncoded = listNameEncoded;
    }

    public String getOldestPublishedDate() {
        return oldestPublishedDate;
    }

    public void setOldestPublishedDate(String oldestPublishedDate) {
        this.oldestPublishedDate = oldestPublishedDate;
    }

    public String getNewestPublishedDate() {
        return newestPublishedDate;
    }

    public void setNewestPublishedDate(String newestPublishedDate) {
        this.newestPublishedDate = newestPublishedDate;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }
}
