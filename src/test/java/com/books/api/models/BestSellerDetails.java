package com.books.api.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class BestSellerDetails {

    @JsonProperty("title")
    private String title;
    @JsonProperty("description")
    private String description;
    @JsonProperty("contributor")
    private String contributor;
    @JsonProperty("author")
    private String author;
    @JsonProperty("contributor_note")
    private String contributorNote;
    @JsonProperty("price")
    private String price;
    @JsonProperty("age_group")
    private String ageGroup;
    @JsonProperty("publisher")
    private String publisher;
    @JsonProperty("isbns")
    private List<Isbns> isbns;
    @JsonProperty("ranks_history")
    private List<RankDetails> ranksHistory;
    @JsonProperty("reviews")
    private List<Reviews> reviews;

    public String getTitle() {
        return title;
    }
}
