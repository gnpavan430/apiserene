package com.books.api.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class BestSellerHistory {

    @JsonProperty("status")
    private String status;
    @JsonProperty("copyright")
    private String copyRight;
    @JsonProperty("num_results")
    private int numResults;
    @JsonProperty("results")
    private List<BestSellerDetails> bestSellerDetails;

    public String getStatus() {
        return status;
    }

    public int getNumResults() {
        return numResults;
    }

    public List<BestSellerDetails> getBestSellerDetails() {
        return bestSellerDetails;
    }
}
