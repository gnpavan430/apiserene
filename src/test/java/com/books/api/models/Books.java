package com.books.api.models;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Books {

    @JsonProperty("status")
    private String status;
    @JsonProperty("copyright")
    private String copyRight;

    @JsonProperty("num_results")
    private int numResults;
    @JsonProperty("last_modified")
    private String lastModified;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("results")
    private List<BookResults> bookResults;

    @JsonAnyGetter
    public String getStatus() {
        return status;
    }

    @JsonAnySetter
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonAnyGetter
    public List<BookResults> getBookResults() {
        return bookResults;
    }

    @JsonAnySetter
    public void setBookResults(List<BookResults> bookResults) {
        this.bookResults = bookResults;
    }

    @JsonAnyGetter
    public int getNumResults() {
        return numResults;
    }
}
