package com.books.api.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Isbns {
    @JsonProperty("isbn10")
    private String isbn10;
    @JsonProperty("isbn13")
    private String isbn13;

}
