package com.books.api.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Reviews {

    @JsonProperty("book_review_link")
    private String bookReviewLink;
    @JsonProperty("first_chapter_link")
    private String firstChapterLink;
    @JsonProperty("sunday_review_link")
    private String sundayReviewLink;
    @JsonProperty("article_chapter_link")
    private String articleChapterLink;

    @JsonAnyGetter
    public String getBookReviewLink() {
        return bookReviewLink;
    }

    @JsonAnyGetter
    public String getFirstChapterLink() {
        return firstChapterLink;
    }

    @JsonAnySetter
    public void setSundayReviewLink(String sundayReviewLink) {
        this.sundayReviewLink = sundayReviewLink;
    }

    @JsonAnyGetter
    public String getArticleChapterLink() {
        return articleChapterLink;
    }

}
