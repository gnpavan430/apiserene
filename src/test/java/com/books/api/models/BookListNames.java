package com.books.api.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class BookListNames {

    @JsonProperty("status")
    private String status;
    @JsonProperty("copyright")
    private String copyRight;

    public String getStatus() {
        return status;
    }

    public String getCopyRight() {
        return copyRight;
    }

    public int getNumResults() {
        return numResults;
    }

    public List<BookName> getBookListNameResults() {
        return bookListNameResults;
    }

    @JsonProperty("num_results")
    private int numResults;

    @JsonProperty("results")
    private List<BookName> bookListNameResults;


}
