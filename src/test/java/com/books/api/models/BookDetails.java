package com.books.api.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BookDetails {
    @JsonProperty("title")
    private String title;
    @JsonProperty("description")
    private String description;
    @JsonProperty("contributor")
    private String contributor;
    @JsonProperty("author")
    private String author;
    @JsonProperty("contributor_note")
    private String contributorNote;
    @JsonProperty("price")
    private String price;
    @JsonProperty("age_group")
    private String ageGroup;
    @JsonProperty("publisher")
    private String publisher;
    @JsonProperty("primary_isbn13")
    private String primary_isbn13;
    @JsonProperty("primary_isbn10")
    private String primary_isbn10;

    @JsonAnyGetter
    public String getTitle() {
        return title;
    }

    @JsonAnyGetter
    public String getDescription() {
        return description;
    }

    @JsonAnyGetter
    public String getAuthor() {
        return author;
    }

}
