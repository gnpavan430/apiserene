Feature: Book List names API

  Background:
    Given a valid books api sessions is created
    Given headers are added to the session
      | key          | value                           |
      | content-type | application/json; charset=UTF-8 |
    When the GET request is made to the api endpoint "ListOfNames"
    Then response is stored in books list name object

  Scenario: Make the generic validations of the books list names response
    Then Response should have status code as 200
    And Response content type should be "application/json; charset=UTF-8"
    And book list names api result should have status as "OK"
    And number of book list names should be greater than 0

  Scenario Outline: Response field validations of Book list names response
    Then response field "listName" with value "<listName>" is present in the response
    Then response field "displayName" with value "<displayName>" is present in the response
    Then response field "updated" with value "<updated>" is present in the response
    Examples:
      | listName          | displayName       | updated |
      | Hardcover Fiction | Hardcover Fiction | WEEKLY  |
      | Audio Nonfiction  | Audio Nonfiction  | MONTHLY |




