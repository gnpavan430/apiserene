Feature: Book List names API negative scenarios

  Scenario: Search for book list names without authorization api token
    Given headers are added to the session
      | key          | value                           |
      | content-type | application/json; charset=UTF-8 |
    When the GET request is made to the api endpoint "ListOfNames"
    Then Response should have status code as 401
    And Response parameter "fault.faultstring" should contain the message "Failed to resolve API Key variable request.queryparam.api-key"
    And Response parameter "fault.detail.errorcode" should contain the message "steps.oauth.v2.FailedToResolveAPIKey"

  Scenario: Search for book list names with invalid authorization api token
    Given headers are added to the session
      | key          | value                           |
      | content-type | application/json; charset=UTF-8 |
    And query params are added to the session
      | key     | value  |
      | api-key | sgsg   |
    When the GET request is made to the api endpoint "ListOfNames"
    Then Response should have status code as 401
    And Response parameter "fault.faultstring" should contain the message "Invalid ApiKey"
    And Response parameter "fault.detail.errorcode" should contain the message "oauth.v2.InvalidApiKey"