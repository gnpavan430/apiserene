Feature: Book Best Sellers History details API

  Scenario Outline: Generic validations of Best sellers details response
    Given a valid books api sessions is created
    Given query params are added to the session
      | key   | value   |
      | isbn  | <isbn>  |
      | title | <title> |
    When the GET request is made to the api endpoint "BestSellersHistory"
    Then response is stored in bestsellers history object
    And Response should have status code as 200
    And number of best seller books is equal to 1
    And best seller history response field "title" with value "<title>" is present in the response
    Examples:
      | isbn          | title                                             |
      | 9780385341004 | THE GUERNSEY LITERARY AND POTATO PEEL PIE SOCIETY |
      | 9780385341004 | THE GUERNSEY LITERARY                             |

  Scenario: Search for list of best seller books without authorization api token
    Given query params are added to the session
      | key   | value                 |
      | isbn  | 9780385341004         |
      | title | THE GUERNSEY LITERARY |
    When the GET request is made to the api endpoint "BestSellersHistory"
    Then Response should have status code as 401
    And Response parameter "fault.faultstring" should contain the message "Failed to resolve API Key variable request.queryparam.api-key"
    And Response parameter "fault.detail.errorcode" should contain the message "steps.oauth.v2.FailedToResolveAPIKey"

  Scenario: Search for list of best seller books with invalid authorization api token
    Given query params are added to the session
      | key     | value                                             |
      | api-key | abcd                                              |
      | isbn    | 9780385341004                                     |
      | title   | THE GUERNSEY LITERARY AND POTATO PEEL PIE SOCIETY |
    When the GET request is made to the api endpoint "BestSellersHistory"
    Then Response should have status code as 401
    And Response parameter "fault.faultstring" should contain the message "Invalid ApiKey"
    And Response parameter "fault.detail.errorcode" should contain the message "oauth.v2.InvalidApiKey"





