Feature: Books API

  Scenario Outline: Get list of Books using list name
    Given a valid books api sessions is created
    And query params are added to the session
      | key  | value            |
      | list | <book_list_name> |
    When the GET request is made to the api endpoint "BooksList"
    Then response is stored in books object
    And Response should have status code as 200
    And Response content type should be "application/json; charset=UTF-8"
    And book api result should have status as "OK"
    And number of books should be greater than 0
    And all the books should have list name as "<bookListName>"
    Examples:
      | bookListName      | book_list_name    |
      | E-Book Fiction    | e-book-fiction    |
      | Hardcover Fiction | hardcover-fiction |

  Scenario: Search for list of books with invalid list name
    Given a valid books api sessions is created
    And query params are added to the session
      | key  | value  |
      | list | sgwbwg |
    When the GET request is made to the api endpoint "BooksList"
    Then response is stored in books object
    Then Response should have status code as 200
    And number of books returned should be 0

  Scenario: Search for list of books without list name parameter
    Given a valid books api sessions is created
    When the GET request is made to the api endpoint "BooksList"
    Then Response should have status code as 400
    And Response parameter "status" should contain the message "ERROR"
    And Response parameter list "errors" has item "Bad Request"
    And Response parameter list "errors" has item "No Parameters specified"

  Scenario: Search for list of books without authorization api token
    Given query params are added to the session
      | key  | value  |
      | list | sgwbwg |
    When the GET request is made to the api endpoint "BooksList"
    Then Response should have status code as 401
    And Response parameter "fault.faultstring" should contain the message "Failed to resolve API Key variable request.queryparam.api-key"
    And Response parameter "fault.detail.errorcode" should contain the message "steps.oauth.v2.FailedToResolveAPIKey"

  Scenario: Search for list of books with invalid authorization api token
    Given query params are added to the session
      | key     | value  |
      | api-key | sgsg   |
      | list    | sgwbwg |
    When the GET request is made to the api endpoint "BooksList"
    Then Response should have status code as 401
    And Response parameter "fault.faultstring" should contain the message "Invalid ApiKey"
    And Response parameter "fault.detail.errorcode" should contain the message "oauth.v2.InvalidApiKey"


